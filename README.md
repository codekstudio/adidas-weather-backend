## Description

NestJS project for adidas weather app challenge.

This repository is part of the "adidas weather app" challenge. To see all project repositories go to https://bitbucket.org/codekstudio/workspace/projects/ADI

The mongo database engine must be correctly installed and running on the local machine so that the app can be record, erase and retrieve data. The name of the database will be adidasWeather.

This repository is the backend of the system and comprises the following main components:

* API Rest: An API has been created with the following endpoints:

  * GET / forecast / city: To obtain the five-day predictions of a given city from the external API. It can be tested locally with:
  ```bash
  curl --location --request GET 'http://localhost:3000/forecasts/Zaragoza
  ```

  * POST / forecast: To put in history (DB Mongo) a prediction of a selected day.

  ```bash
    curl --location --request POST 'http://localhost:3000/forecasts' \
    --header 'Content-Type:application/json' \
    --data-json '{
     "date": "2021-07-04T18: 00: 00.000Z",
     "temp": 23,
     "maxTemp": 30,
     "minTemp": 15,
     "main": "Rains",
     "description": "Scattered storms",
     "icon": "i09",
     "city": "Zaragoza"'
  ```

  * GET /forecast?offset=[xxx]&limit=[xxx] : To obtain a series of records from the local history.
  ```bash
  curl --location --request GET 'http://localhost:3000/forecasts?offset=3&limit=1'
  ```

  * GET / forecast / id. To obtain a saved daily prediction from the history.
  ```bash
  curl --location --request GET 'http://localhost:3000/forecasts/forecast/[idMongo]'
  ```

  * DELETE / forecast / id. To delete a saved daily prediction from the history.
  ```bash
  curl --location --request DELETE 'http://localhost:3000/forecasts/forecast/[idMongo]'

  ```
* Service for external API call ("weather-external.service").

* Mqtt client to connect to the mqtt message broker when temperatures greater than 40ºC are detected and in this way that any system can connect to the warning message system.

* Websocket to communicate the previous notice directly to the frontend. This component is optional since the frontend client could connect directly to the broker to receive notices in real time.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```


## To be done

Due to lack of time the system is not complete. Some main elements to be completed are noted here:

* API documentation: The NestJS swagger-ui-express package has been installed, which automatically generates the documentation through annotations following the openAPI standard. Therefore, it would be necessary to activate said library in the main module and write down the corresponding endpoints in the "forecasts.controller.ts" controller. With this and adding annotated text, the documentation is generated for the use of the API.
