import { Module, HttpModule } from '@nestjs/common';
import { WeatherExternalService } from './v1/common/services/weather-external.service';
import { ForecastsController } from './v1/forecasts/forecasts.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ForecastService } from './v1/forecast/forecast.service';
import { ForecastSchema } from './v1/forecast/forecast.schema';
import { WebsocketModule } from './v1/common/websocket/websocket.module';
import { MqttService } from './v1/common/services/mqtt.service';

@Module({
  imports: [
    HttpModule, 
    MongooseModule.forRoot('mongodb://localhost/adidasWeather'),
    MongooseModule.forFeature([{ name:'Forecast', schema: ForecastSchema }]),
    WebsocketModule
  ],
  controllers: [ForecastsController],
  providers: [WeatherExternalService, ForecastService, MqttService]
})
export class AppModule {
  constructor(
    private readonly mqttService: MqttService
  ) {}

  onModuleInit() {
    console.log(`Starting MQTT client...`);
    this.mqttService.init();
  }
}
