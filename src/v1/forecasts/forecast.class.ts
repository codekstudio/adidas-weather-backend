import { CForecast } from '../forecast/forecast.class';

export class CForecasts {
    forecasts: []

    constructor(externalForecasts: any) {
        const forecasts: [] =  externalForecasts.list.map(extForecast => {
            const forecast = new CForecast(extForecast);
            forecast.city = externalForecasts.city.name;
            return forecast;
        })

        return {
            forecasts
        }
    }
}
