import * as mongoose from 'mongoose';

export const ForecastsSchema = new mongoose.Schema({
    forecasts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Forecast' }]
}, {
    timestamps: true,
});
