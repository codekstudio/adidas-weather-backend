import { Controller, Get, Query, Body, Post, Param, Delete } from '@nestjs/common';
import { WeatherExternalService } from '../common/services/weather-external.service';
import { CForecasts } from './forecast.class';
import { Forecast } from '../forecast/forecast.interface';
import { ForecastService } from '../forecast/forecast.service';


@Controller('forecasts')
export class ForecastsController {
    constructor(
        private readonly weatherExternalService: WeatherExternalService,
        private readonly forecastService: ForecastService
    ) {}

    @Get(':city')
    getForecast(@Param('city') city: any) { 
        return this.weatherExternalService.getForecast(city)
            .then(externalForecasts => {
                const forecasts = new CForecasts(externalForecasts.data);
                return forecasts;
            })
            .catch(r => console.log(r))
    }

    @Post()
    createForecast(@Body() forecast: Forecast) { 
        return this.forecastService.create(forecast)
            .then(createdForecast => createdForecast)
            .catch(r => console.log(r))
    }

    @Get()
    getHistForecast(@Query('offset') offset: number, @Query('limit') limit: number) { 
        return this.forecastService.findAll(offset, limit)
            .then(histForecasts => histForecasts)
            .catch(r => console.log(r))
    }

    @Get('forecast/:id')
    getHistForecastById(@Param('id') id: string) { 
        return this.forecastService.findById(id)
            .then(forecast => forecast)
            .catch(r => console.log(r))
    }

    @Delete('forecast/:id')
    removeHistForecastById(@Param('id') id: string) {
        return this.forecastService.removeById(id)
            .then(forecast => forecast)
            .catch(r => console.log(r))
    }

}
