import { Injectable } from '@nestjs/common';
import * as mqtt from 'mqtt';
import { WebGateway } from '../websocket/websocket.gateway';

@Injectable()
export class MqttService {
    host: string;
    port: string;
    protocol: string;
    clientId: string;
    reconnectPeriod: string;
    keepalive: string;
    rejectUnauthorized: string;
    willTopic: string;
    willPayload: string;
    willReatin: string;
    tiempoPasado: number = 0;
    tiempoActivo: boolean = true;

    client: any

    constructor(private readonly webGateway: WebGateway) {}

    init() {
        this.host = '127.0.0.1'
        this.port = '8883'
        this.protocol = 'mqtts'
        this.clientId = 'adidas-backend-' + Math.random().toString(16).substr(2, 8);
        this.reconnectPeriod = '1000'
        this.keepalive = '300'
        this.rejectUnauthorized = 'false'
        this.willTopic = 'adidas/core/backend/status'
        this.willPayload = 'Offline'
        this.willReatin = 'true'

        this.client = mqtt.connect({
            host: this.host,
            port: this.port,
            protocol: this.protocol,
            clientId: this.clientId,
            reconnectPeriod: this.reconnectPeriod,
            keepalive: +this.keepalive, 
            rejectUnauthorized: false,
            clean: true,
            will: {
                topic: this.willTopic,
                payload: new Buffer(this.willPayload),
                retain: this.willReatin
            }
        })

        this.connect();
    }

    private connect() {
        console.log(this.client.options.keepalive);
        this.client.on('connect', () => {
            console.log(`Connected to aedes broker at  ${this.host} on port ${this.port}`);
            this.sedStatusMessage()
            this.subscriptions();
        })
    }

    private subscriptions() {
        const options = {
            qos: 0
        }

        this.client.subscribe('adidas/core/#', options, (err, granted) => {
            if (err) {
                console.error('Error in subscription adidas/core/#: ' + err);
            } else {
              console.log('Successfully subscribed to adidas/core/# with: ' +  JSON.stringify(granted));
              this.listen();
            }
        })
    }

    private listen() {
        this.client.on('message', async (topic, message, packet) => {
            const topicS: string = topic.toString();
            const messageS: string = message.toString().trim(); 

            console.log('--------- Entering messsage ----------');
            console.log(JSON.stringify(packet));
            console.log(topicS);
            console.log(messageS);

            this.webGateway.server.emit('event', messageS);
        })
    }

    private sedStatusMessage() {
        const options = {
            retain: true
        };
        this.client.publish('adidas/core/backend/status', 'Online', options, (err) => {
            console.log(err);
        });
        console.log('Status sent');
    }
    
}

