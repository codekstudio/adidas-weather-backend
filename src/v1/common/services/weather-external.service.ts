import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/common';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';

@Injectable()
export class WeatherExternalService {
    constructor(private readonly httpService: HttpService) {}

    getForecast(city): Promise<AxiosResponse> {
        return this.httpService.get(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&APPID=d76867ff5907a85f56a066fa4afcf624&units=metric`).toPromise()
    }
}
