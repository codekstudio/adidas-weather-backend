import { Module } from '@nestjs/common';
import { WebGateway } from './websocket.gateway';

@Module({
    providers: [ WebGateway ],
    exports: [ WebGateway ]
})
export class WebsocketModule {}
