import { WebSocketGateway, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';

@WebSocketGateway()
export class WebGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() server;
    clients = 0;

    async handleConnection(){
        this.clients++;
        this.server.emit('clients', this.clients);
    }

    async handleDisconnect(){
        this.clients--;
        this.server.emit('clients', this.clients);
    }
}