import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type ForecastDocument = Forecast & Document;

@Schema({
    timestamps: true,
})
export class Forecast {
  @Prop()
  date: Date;

  @Prop()
  temp: number;

  @Prop()
  maxTemp: number;

  @Prop()
  minTemp: number;

  @Prop()
  main: string;

  @Prop()
  description: string;

  @Prop()
  icon: string;

  @Prop()
  city: string;
}

export const ForecastSchema = SchemaFactory.createForClass(Forecast);