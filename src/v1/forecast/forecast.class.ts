export class CForecast {
    date: Date;
    temp: number;
    maxTemp: number;
    minTemp: number;
    main: string;
    description: string;
    icon: string;
    city: string;

    constructor(openWeatherForecast: any) {

        return {
            date: new Date(openWeatherForecast.dt*1000),
            temp: openWeatherForecast.main.temp,
            maxTemp: openWeatherForecast.main.temp_max,
            minTemp: openWeatherForecast.main.temp_min,
            main: openWeatherForecast.weather[0].main,
            description: openWeatherForecast.weather[0].description,
            icon: openWeatherForecast.weather[0].icon,
            city: openWeatherForecast.city,
        }
    }
}
