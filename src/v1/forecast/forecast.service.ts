import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Forecast, ForecastDocument } from './forecast.schema';

@Injectable()
export class ForecastService {
  constructor(@InjectModel('Forecast') private forecastModel: Model<ForecastDocument>) {}

  async create(forecast: Forecast): Promise<Forecast> {
    const createdForecast = new this.forecastModel(forecast);
    return createdForecast.save();
  }

  async findAll(offset = 0, limit = 0): Promise<Forecast[]> {
    return this.forecastModel.find().skip(+offset).limit(+limit).exec();
  }

  async findById(id: string): Promise<Forecast> {
    return this.forecastModel.findById(id).exec();
  }

  async removeById(id: string): Promise<any> {
    return this.forecastModel.findByIdAndRemove(id).exec();
  }
}
