export interface Forecast {
     date: Date;
     temp: number;
     maxTemp: number;
     minTemp: number;
     main: string;
     description: string;
     icon: string;
     city: string;
}